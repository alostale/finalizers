package org.alo.test.finalizers;

import java.util.ArrayList;
import java.util.List;

public class Main {

  public static void main(String[] args) throws InterruptedException {
    System.out.println("Xmx: " + Runtime.getRuntime().maxMemory() / 1048576 + "M");
    List<DummyClass> retainer = new ArrayList<>(10_000);
    for (int i = 0; i < 10_000; i++) {
      if (true) { // change this to compare with objects not implementing finalizable
        var finalizable = new FinalizableClass();
        if (true) { // retain finalizables
          retainer.add(finalizable);
        }
      } else {
        new DummyClass();
      }
    }
    waitAndLog(100);

    System.out.println("gc");
    System.gc();

    waitAndLog(100);
    System.out.println(retainer);

  }

  private static void waitAndLog(int secs) throws InterruptedException {
    System.out.println("sleep");
    for (int i = 0; i < secs; i++) {

      System.out.println("finalized " + FinalizableClass.finalizedCnt + " instances");
      Thread.sleep(1_000);
    }

  }

}
