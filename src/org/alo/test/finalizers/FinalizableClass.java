package org.alo.test.finalizers;

public class FinalizableClass extends DummyClass {

  @Override
  protected void finalize() throws Throwable {
    DummyClass.finalizedCnt += 1;
    // System.out.println("finalizing " + instanceNo + " - " + finalizedCnt);
  }
}
